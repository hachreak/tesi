\chapter{Esperimenti}

Per accertare la bontà di un modello bisogna descrivere un esperimento nel dettaglio e ripetere la sua esecuzione un numero sufficientemente alto di volte.

Per limitare la dipendenza dei risultati dalle immagini scelte viene sempre usata la tecnica della 10-Fold Cross Validation.

Inoltre, lo stesso esperimento viene ripetuto più volte, solitamente dieci, usando un seme diverso per ogni configurazione della Cross Validation.

Ogni esperimento si compone delle seguenti fasi:

\begin{description}
	\item[Init] Nella fase di inizializzazione viene impostato un seme per il generatore di numeri pseudo casuale a scelta.
	Successivamente viene scelto il numero di blocchi in cui dividere il dataset, dividendolo in tre parti distinte ed equilibrate rispetto al numero di classi.
	Vengono così definiti i test set, validation set e training set.
	\item[Pre-processing] Ogni dataset ha normalmente bisogno di una fase iniziale di pre-elaborazione che dipende dalle caratteristiche di ciascuno dei dataset inclusi.
	
	Questa fase solitamente è eseguita off-line, in quanto le operazioni richieste sono invarianti rispetto alle fasi successive. Alla fine del processo si ottiene un unico set omogeneo utilizzabile per addestrare e valutare la rete. 
	
	Siccome il processo di addestramento che farà uso delle immagini elaborate viene ripetuto diverse volte, il risultato dell'elaborazione viene salvato in una cartella che serve da cache temporanea. 
	
	Le operazioni possono coinvolgere sia l'immagine che le etichette. RemoKit mette a disposizione tutta una serie di utilità per comporre in modo dichiarativo tutti i vari passaggi richiesti.
	\item[Post-processing] Questa fase si compone di ulteriori elaborazioni sul dataset, questa volta eseguite online, ossia tutte le volte che avviene un addestramento o predizione. 
	
	Il risultato finale è uno stream di immagini e etichette raccolte in batch direttamente utilizzabili dalla funzione di addestramento e/o di valutazione. La scelta di usare generatori Python garantisce all'esperimento il supporto a dataset potenzialmente infiniti. 
	Basterà scegliere un valore di batch size opportuno per limitare il quantitativo  massimo di memoria utilizzata.
	Quando l'elaborazione su batch termina, la memoria viene liberata per lasciare spazio al prossimo batch. 
	\item[K-fold cross validation] Il dataset viene diviso in tre parti seguendo i principi del metodo K-fold cross validation: training set, validation set e test set. Il primo grande k-2 parti e i restanti file equamente divisi tra validation e test.
	\item[Training] Questa fase comporta l'istanziazione degli stream di dati per il training set e per il validation set. 
	
	Inoltre viene compilato il modello di rete neurale utilizzando come funzione di loss la ``categorical crossentropy'', utile per la classificazione multi-etichetta delle sette emozioni previste.
	
	Come ``optimizer'' della rete viene usato di default l'algoritmo Adam con un learning rate configurabile.
	Inoltre, durante il tranining sono configurati due importanti callback:
	\begin{description}
		\item[Early Stop Callback] Questa callback permette di bloccare il training della rete neurale se un parametro si ritrova ad avere certe caratteristiche.
		
		Di default il parametro da monitorare è la ``validation loss'', ma è possibile configurare il parametro che si vuole. 
		
		Impostando il ``patience'' a 10, se il parametro da monitorare non varia per le successive 10 epoche, il training viene fermato prematuramente.
		\item[Reduce Learning Rate on Plateau] Come per la Early Stop, anche questa callback viene configurata per monitorare un parametro, ma, a differenza della precedente, viene semplicemente abbassato il Learning Rate generale. 
		
		Di default il parametro da monitorare è la ``validation loss'', ma è possibile configurare il parametro che si vuole. Partendo da un valore di 0.001 e ponendo una ``patience'' a 5, se la validation loss non varia per le successive 5 epoche, il learning rate viene dimezzato. Così, il parametro viene diminuito fino ad arrivare a un minimo di 0.0001.
	\end{description}
	\item[Evaluation] Questa fase prevede la valutazione della bontà dell'addestramento eseguendo una predizione su un test set. Alla fine della valutazione vengono collezionate tutte le metriche disponibili: matrice di confusione, precision, recall, f1-score, storia del valore della loss e dell'accuratezza durante tutta la fase di addestramento.
\end{description}

I dataset utilizzati negli esperimenti seguenti sono quattro in totale.

In tabella \ref{table:caratteristiche_datasets} viene descritta la suddivisione tra le varie emozioni.

\begin{table}[!htbp]
	\centering
	\begin{tabular}{c|c|ccccccc}
		\toprule
		Dataset & Totale & Af & An & Sa & Ne & Di & Su & Ha \\
		\midrule
		KDEF & 980 & 140 & 140 & 140 & 140 & 140 & 140 & 140 \\
		CK+ & 616 & 25 & 45 & 28 & 308 & 58 & 83 & 69 \\
		JAFFE & 210 & 31 & 30 & 31 & 29 & 29 & 29 & 31 \\
		PICS & 322 & 45 & 46 & 46 & 47 & 48 & 45 & 45 \\
		\bottomrule
	\end{tabular}
	\caption{Caratterizzazione dei dataset usati negli esperimenti}
	\label{table:caratteristiche_datasets}
\end{table}

\section{Studio sulla profondità della rete}

Uno dei parametri fondamentali che descrive una rete convoluzionale è il numero di livelli di convoluzione che la rete possiede.
Solitamente, questi formano il primo stadio della rete neurale.

La scelta di un valore appropriato pregiudica fortemente la qualità dei risultati finali, in quanto una rete troppo piccola non sarà in grado di avere un sufficiente potere discriminativo.

Al contrario, la scelta di una rete troppo grande potrebbe avere effetti negativi in caso la quantità e qualità degli elementi del training set posseduto non fosse sufficiente elevata.
In questo caso, l'effetto più comune di cui si fa esperienza è l'\emph{underfitting}, ossia l'incapacità di addestrare sufficientemente la rete.

Non esistendo formule chiuse che permettano di individuare la configurazione ottimale, si è scelto di procedere per tentativi, mettendo insieme i dataset disponibili e testando diverse configurazioni.

In tabella \ref{table:exp01} sono messe a confronto quattro differenti architetture: 

\begin{table}[!htbp]
	\centering
	\begin{tabular}{cp{6cm}ccc}
		\toprule
		Modello & Caratteristiche & \multicolumn{3}{c}{Accuratezza} \\
		\midrule
		{} & {} & Minimo & Media & Massimo \\
		03 &  2 strati Conv2D & 0.66 & 0.75 & 0.86  \\
		04v2 & 3 strati Conv2D + regolatori l2 & 0.73 & 0.81 & 0.87 \\
		05 & 4 strati Conv2D + regolatori l2 & 0.78 & 0.85 & 0.9 \\
		08 & 1 strati Conv2D + 2 inception + AveragePooling & 0.58 & 0.73 & 0.85 \\
		\bottomrule
	\end{tabular}
	\caption{Risultati ottenuti dalle diverse reti per l'esperimento 01}
	\label{table:exp01}
\end{table}

Sono stati utilizzati tre dataset diversi: KDEF, CK+, JAFFE e PICS.
Per un totale di 1806 espressioni divise in: 196 afraid, 215 angry, 199 sad, 477 neutral, 227 disgust, 252 surprised e 240 happy.

I risultati sono stati successivamente confrontati per poter giungere a una conclusione.

In figura \ref{figure:exp01_accuracy_boxplot} abbiamo, in ordine, l'esperimento 01 (modello 03), l'esperimento 02 (modello 04v2), l'esperimento 03 (modello 05) e l'esperimento 04 (modello 08).

Dal grafico risulta evidente che la favorita è la rete 05 con 4 strati convolutivi.

\begin{figure}
	\centering
	\includegraphics[width=14cm]{images/exp01_accuracy_boxplot.png}
	\caption{Confronto tra le varie reti nell'esperimento 01}
	\label{figure:exp01_accuracy_boxplot}
\end{figure}

In figura \ref{figure:exp01_cms} sono presentate le diverse matrici di confusione.
Se ordiniamo la percentuale di veri positivi dal valore più alto al più basso, ci accorgiamo che tutti gli esperimenti presentano lo ordine: happy,  neutral, surprised, disgust, angry, sad, afraid.
A parte il modello 08 che presenta le ultime due classi scambiate.
Questa caratteristica ci suggerisce che, indipendentemente dal dataset, ci sono alcune emozioni oggettivamente più difficili da distinguere.

Inoltre, notiamo anche che le classi che hanno ottenuto i risultati peggiori in tutti e quattro i casi sono ``sad'' e ``afraid'', che sono anche i casi meno rappresentati.

\begin{figure}
	\centering
	\includegraphics[width=14cm]{images/01_face_multidataset_cms.png}
	\caption{Matrici di confusioni ottenute nell'esperimento 01}
	\label{figure:exp01_cms}
\end{figure}

Vediamo in dettaglio i risultati ottenuti dal modello 08.

Possiamo notare come l'utilizzo di una rete più complessa con moduli Inception non abbia portato vantaggi.

La rete con cui si può effettuare una diretta comparazione è la rete 04v2, in quanto anch'essa dispone di tre macro strati.

Rispetto a questa rete però, il secondo e terzo strato sono molto più complessi e portano ad un complessivo raddoppio del numero di parametri, passando da quasi sette milioni e mezzo a più di quindici milioni.

Il calo del valore minimo si può attribuire proprio a questa complessità aggiuntiva, richiedendo a chi addestra un dataset più grande per poter far fronte a questo costo aggiuntivo.

Il modulo Inception nasce per poter far fronte ad esempio a cambi di luminosità e dimensione del soggetto, cosa che non avviene nei dataset utilizzati, i quali hanno caratteristiche molto stringenti e non presentando questo tipo di varietà.
Questo giustificherebbe il calo di prestazioni ottenuto.


\section{Studio sulla quantità e qualità delle immagini}

Come già anticipato nello studio precedente, la quantità di immagini presente nel dataset ha un effetto non trascurabile sulla scelta dell'architettura della rete neurale.
Di seguito viene presentato uno studio mirato a mettere in luce questa relazione.

Inoltre, verrà stimato anche come la qualità occupi un posto determinante nella ricerca dei giusti iperparametri della rete.

Il modello di rete utilizzato per l'esperimento è la 04, con tre strati convolutivi, e la 05, con quattro strati.

Sono stati utilizzati quattro dataset: KDEF, CK+, JAFFE, PICS.

\subsection*{Sessione 01: singoli dataset}

La prima parte dell'esperimento si compone di quattro sessioni che utilizzano la rete 04, atte a valutare la difficoltà intrinseca di ogni dataset.

Ogni selezione di immagini ha, come vedremo, caratteristiche che incidono sulle prestazioni finali.
A livello visivo possono non essere percettibili grosse differenze, ma all'atto pratico può rivelarsi presente un insieme di caratteristiche che inficiano le capacità di scelta della rete.

Valutare questi dettagli è necessario per poter prendere le giuste decisioni in ambito architetturale.

\begin{figure}
	\centering
	\includegraphics[width=12cm]{images/02_accuracies_singledatasets.png}
	\caption{Accuratezza sui singoli dataset da parte della rete 04}
	\label{figure:exp02_accuracies_singletadatasets}
\end{figure}

In figura \ref{figure:exp02_accuracies_singletadatasets} notiamo come, a parità di rete convolutiva utilizzata per classificare, i dataset presentino una difficoltà media abbastanza simile, a parte il dataset $PICS$ che si rivela essere decisamente più ostico.

Trovare un sistema per rilevare in modo efficace e preventivo la difficoltà potrebbe essere uno strumento utile nella fase di scelta degli iperparametri.

Potrebbe avvenire, come visto, attraverso l'esecuzione di un intero ciclo di training-predict con l'uso di una rete di prova.

Oppure, per esempio potrebbe rivelarsi utile una analisi attraverso l'uso della PCA (Principal Component Analysis).
I vantaggi rispetto ad una rete neurale sono multipli: riduzione a un solo parametro di configurazione e tempi necessari per ottenere il risultato ridotti.

Come noto, la PCA è un sistema molto popolare di riduzione della dimensionalità dei dati.
Detto in altre parole, più i dati contengono informazioni rilevanti e più sarà difficile comprimerli in uno spazio più piccolo.

Ovviamente, è difficile parlare di una vera e propria relazione tra difficoltà e riduzione dimensionale.
Nel nostro caso però, può essere utile per avere una indicazione sulla quantità di informazione esposta dalle immagini.
Se l'informazione intrinseca nell'immagine risultasse essere scarsa, possiamo immaginare che anche una rete neurale ne risentirà al momento decisivo in cui dovrà predirne l'emozione corrispondente.

Nella tabella \ref{table:exp02_datasets_pca} sono esposti i risultati dell'analisi.

Notiamo come $KDEF$ e $CK+$ abbiano ottenuto lo stesso valore, in sintonia con quanto visto nella figura \ref{figure:exp02_accuracies_singletadatasets}.
La $JAFFE$ si conferma quella più difficile da comprimere e più facile da predire.
Infine, $PICS$ si è rilevata quella con un fattore di compressione più alto, in accordo anch'essa con il risultato di accuratezza più basso.

\begin{table}[!htbp]
	\centering
	\begin{tabular}{c|c}
		\toprule
		Dataset & Risultato della PCA \\
		\midrule
		KDEF & 150x7 \\
		CK+ & 150x7 \\
		JAFFE & 150x10 \\
		PICS  & 150x6 \\
		\bottomrule
	\end{tabular}
	\caption{Risultati sulla riduzione della dimensionalità dei dati con la PCA a 0.95}
	\label{table:exp02_datasets_pca}
\end{table}

\subsection*{Sessione 02: incremento del dataset}

L'esperimento si compone di quattro sessioni di training e valutazione, dove ogni volta viene aggiunto un dataset nuovo.

Attraverso una pipeline apposita vengono unificati i dataset in modo da poter così eseguire le prove. 

In tabella \ref{table:exp02_datasets} vengono descritte le quattro sessioni.
La scelta dell'ordine di inserimento dei dataset non è casuale, ma frutto dei risultati presentati precedentemente sulla qualità dei dataset.

\begin{table}[!htbp]
	\centering
	\begin{tabular}{c|c|l}
		\toprule
		Sessione & Modello rete & Dataset usati \\
		\midrule
		0 & 04 & PICS \\
		1 & 04 & PICS e KDEF \\
		2 & 04 & PICS, KDEF e CK+ \\
		3 & 04 & PICS,  KDEF, CK+, JAFFE \\
		\bottomrule
	\end{tabular}
	\caption{Corrispondenza tra sessione dell'esperimento 02 con i dataset usati.}
	\label{table:exp02_datasets}
\end{table}

In tabella \ref{table:exp02_quantita_immagini_datasets} si possono osservare nel dettaglio la quantità di immagini usate, in congiunzione alla loro distribuzione rispetto alle singole classi.
L'ultima informazione ci dice invece di quanto il dataset sia incrementato rispetto alla sessione precedente.

\begin{table}[!htbp]
	\centering
	\begin{tabular}{c|c|ccccccc|c}
		\toprule
		Sessione & Totale & Af & An & Sa & Ne & Di & Su & Ha & \\
		\midrule
		0 & 322 & 35 & 46 & 46 & 47 & 48 & 45 & 45 & \\
		1 & 1302 & 186 & 186 & 187 & 188 & 185 & 185 & 185 & +980 \\
		2 & 1928 & 210 & 231 & 214 & 495 & 246 & 268 & 254 & +938 \\
		3 & 2128 & 241 & 261 & 245 & 524 & 275 & 297 & 285 & +200 \\
		\bottomrule
	\end{tabular}
	\caption{Quantità di immagini nel dataset ad ogni esecuzione}
	\label{table:exp02_quantita_immagini_datasets}
\end{table}

Ogni sessione si compone come sempre dell'esecuzione di $10 x 10$ volte, usando le due tecniche già descritte di K-Fold Cross Validation con $K=10$ ed esecuzione multipla (10 volte) con cambio di seme nel generatore pseudo-casuale.

In figura \ref{figure:exp02_accuracies} vengono mostrati i primi risultati.	

Si nota subito che aumentare il numero di immagini comporta un beneficio.
Questo ovviamente se le nuove arricchiscono sufficientemente il dataset.

\begin{figure}
	\centering
	\includegraphics[width=12cm]{images/02_accuracies.png}
	\caption{Confronto tra i valori di accuratezza ottenuti nell'esperimento 02}
	\label{figure:exp02_accuracies}
\end{figure}

L'introduzione di nuovi dataset con un apporto informativo maggiore ha portato un benefico.

\section{Studio della rete Multi Stacked NN}

Le reti precedentemente descritte ottengono discreti risultati, arrivando ad avere un'accuratezza media che supera la soglia dell'80\%.

Sarebbe utile a questo punto conoscere qualche dettaglio più preciso su come la rete percepisce le immagini di ingresso, questo ci permetterebbe di avere qualche indizio su come migliorare ulteriormente le prestazioni.

Viene esposto in figura \ref{figure:saliency_map_face} il risultato dell'uso delle Saliency Map \cite{2013arXiv1312.6034S}.
In questo modo è possibile vedere dove la rete concentra la propria attenzione.
Intuitivamente ciò dovrebbe evidenziare le regioni di immagine salienti, cioè quelle che più contribuiscono all'output.
Come si può notare, la rete giustamente mette in risalto i lineamenti che compongo il volto, con particolare attenzione rivolta verso una sola parte della bocca, ignorando per esempio parte degli occhi.

La domanda che ci poniamo è ovviamente la seguente: come possiamo indirizzare in maniera mirata l'attenzione della rete verso parti del viso più espressive?

L'idea proposta nel seguente esperimento è semplice: per fare in modo che la rete si concentri sui dettagli che vogliamo, ritagliamo questi ultimi dall'immagine e proponiamo solo quelli alla rete durante l'addestramento.

\begin{figure}
	\centering
	\includegraphics[width=12cm]{images/03_05_saliency/face.png}
	\caption{Saliency Map applicata al volto}
	\label{figure:saliency_map_face}
\end{figure}

Le zone del volto a più alto valore informativo, come possiamo facilmente immaginare, sono gli occhi e la bocca.
Avremo così diverse reti specializzate che confluiranno insieme le proprie conoscenze.
Questo vuol dire che, oltre ad avere una rete il cui compito è elaborare il volto intero, avremo reti CNN focalizzate sui dettagli più rilevanti.

Per poter mettere insieme tutte le conoscenze così acquisite c'è bisogno di una ulteriore rete che avrà il compito di raccogliere i risultati di ogni rete specializzata ed emettere una predizione finale.

Per evitare una esplosione in numero di neuroni del layer di input di questa ultima rete, si è deciso di non raccogliere il risultato delle reti convoluzionali, bensì delle predizioni.
Questo ci garantisce un incremento lineare contenuto al crescere del numero di sotto-reti.
Inoltre, avremo tutta l'informazione che ci serve compressa nelle sette probabilità in uscita.

Per capire meglio il meccanismo alla base di questa particolare architettura, ci ricolleghiamo al concetto di Inception proposto Szegedy at al \cite{2014arXiv1409.4842S}.
In quel caso, filtri convoluzionali di grandezza crescente vengono messi in parallelo per catturare dettagli a scale diverse, lasciando alla rete il compito di selezionare quelli più significativi.

Nel nostro caso, invece che avere singoli filtri, abbiamo intere reti convoluzionali addestrate su parti dell'immagine diverse.
Va da sé che la conoscenza acquisita da ognuna sarà inevitabilmente diversa.
Ma questo può solo essere un vantaggio, in quanto potremmo capitare in uno dei seguenti casi fortunati:

\begin{enumerate}
	\item Una delle reti può rivelarsi più brava di altre in particolari condizioni e quindi darci da sola la risposta corretta.
	\item Ognuna di esse può esprimere una forte incertezza che non permetterebbe da sola di ricavare una predizione valida; in questo caso però, potrebbe accadere che, mettendo insieme i diversi risultati, si possa inferire una nuova relazione input/output.
	\item Solo una parte delle sotto-reti sbaglia la sua predizione. In questo caso, attraverso un meccanismo dinamico di bilanciamento si potrebbe comunque ottenere la risposta corretta.
\end{enumerate}

Se i risultati fossero considerati singolarmente, non si avrebbe nessun strumento per superare l'incertezza della predizione.
Quello che si vuole mettere in luce è proprio questo aspetto, in cui i risultati dei singoli potrebbero nascondere dentro di sé l'informazione latente utile ma non di facile utilizzo senza uno strumento che ne valorizzi i pregi dinamicamente.

Come nel modulo Inception, queste informazioni estratte in parallelo saranno poi accorpate e sintetizzate per trarne una conclusione finale. 
Nella nostra rete, questo compito spetta alla rete ``fully connected'' finale, la quale cercherà un pattern comune tra le diverse predizioni.

In figura \ref{figure:multi_stack} vediamo la rappresentazione della rete.
Come si vede, lo step-1 si compone di una serie di reti neurali in parallelo, le cui predizioni vengono concatenate verso lo step-2 composto da una ulteriore rete di classificazione.

\begin{figure}
	\centering
	\includegraphics[width=12cm]{images/multistack.png}
	\caption{Rappresentazione di una rete Multi Stacked}
	\label{figure:multi_stack}
\end{figure}

L'esperimento si compone di cinque sessioni diverse, in ognuna delle quali viene addestrata una rete Multi Stacked, con un numero crescente di sotto-reti.
Valutate le prestazioni, verranno confrontate le accuratezze così ottenute, comparandole con una rete CNN classica addestrata solamente sul volto.

In particolare, sono state prese in esame cinque possibili reti che si addestrano su: il volto, gli occhi, la bocca, la congiunzione tra occhi e bocca e infine le feature del volto.

Ovviamente, la dimensione dello strato di input di ogni rete cambia a seconda di quale parte si è scelto.
In tabella \ref{table:input_shape_step1} vengono schematizzate le dimensioni dello strato di input in base al tipo di immagine. 

\begin{table}[!htbp]
	\centering
	\begin{tabular}{ll}
		\toprule
		Immagine/Matrice di input & Dimensione \\
		\midrule
		Faccia & 150x150 \\
		Occhi & 63x150 \\
		Bocca & 110x150 \\
		Occhi + Bocca & 150x126 \\
		Features & 150x150 \\
		\bottomrule
	\end{tabular}
	\caption{Input Shape per le reti dello step-1}
	\label{table:input_shape_step1}
\end{table}


\subsection{Fase di addestramento}

Per poter addestrare una rete di questo tipo c'è bisogno di una procedura dedicata, a più livelli.

Possiamo identificare due fasi distinte: addestramento delle reti dello step-1 e, successivamente, addestramento della rete dello step-2.

In una fase iniziale, il dataset viene trasformato in base a che tipo di rete si vuole addestrare.
Attraverso una pipeline di pre-processing si estraggono le parti del viso che verranno così a formare nuovi dataset.
Nel caso delle feature, il risultato non è una immagine ma una matrice di punti nel formato ``.npy'' di Numpy.

Successivamente, per ogni dataset così composto, viene inizializzata una nuova pipeline di post-processing che servirà ad alimentare la rete neurale nella sua fase di addestramento.

Queste operazioni potranno essere eseguite in sequenza oppure anche in parallelo in quanto indipendenti l'una dall'altra.
Questo permette di poter meglio scalare orizzontalmente in caso sia necessario velocizzare questa fase.

Nella seconda fare dell'addestramento vengono ricostruite le pipeline di post-processing di ogni sotto-rete, aggiungendo come tassello finale l'esecuzione dell'operazione di predizione della rete stessa.

Si vengono così a formare cinque flussi diversi per ciascuno dei quali il risultato finale sono le sette probabilità associate alle sette classi di emozioni.
Questi flussi verranno unificati in un singolo flusso passato come input per l'addestramento della rete ``fully connected'' dello step-2.
Lo strato di input di questa seconda rete sarà così formato da 35 neuroni, ognuno rappresentante una probabilità.
In uscita alla rete saranno presenti le solite sette probabilità sulle sette classi, addestrate sulle conoscenze delle reti dello step-1.

\subsection{Fase di predizione}

Per avere una predizione ci sarà così bisogno di una fase intermedia in cui tutte le reti dello step-1 esprimono le proprie considerazioni.
Successivamente, la rete principale dello step-2 ritorna quella che è la predizione finale.
Questo vuol dire che ci sarà bisogno di estrarre a runtime le parti del volto e alimentare così la fase intermedia.

I risultati ottenuti come vedremo sono migliori rispetto a una rete CNN a uno stadio solo, a discapito però della velocità di esecuzione in fase di classificazione che risulta dimezzata nel caso migliore.

\subsection{Risultati}

Nella tabella \ref{table:exp03_results} vengono esposte le cinque sessioni diverse dell'esperimento messe in comparazione con i risultati ottenuti da una rete CNN normale che elabora il volto.
Come si può notare, all'aumentare del numero di reti dello step-1 corrisponde anche un incremento della media dell'accuratezza.

In figura \ref{figure:exp03_accuracies_all} è presentata una visualizzazione più dettagliata della distribuzione delle accuratezze, che ci fa notare come l'incremento delle prestazioni è legato molto anche all'apporto di informazioni fresche dato dalla sotto-rete.
Per esempio, una rete Eyes/Mouth in aggiunta alle due reti che elaborano separatamente gli occhi e la bocca non porta un grande quantitativo di informazioni nuove. 
Infatti notiamo che la media non incrementa di molto.
La rete che elabora le feature, invece, non ha un particolare effetto in nessuno dei due casi.

\begin{table}[!htbp]
	\centering
	\begin{tabular}{c|l|ccc}
		\toprule
		Sessione & Reti & \multicolumn{3}{c}{Accuratezza} \\
		{} & {} & Minimo & Media & Massimo \\
		\midrule
		  & Face & 0.67 & 0.77 & 0.88 \\
		0 & Face + Mouth & 0.66 & 0.78 & 0.89 \\
		1 & Face + Mouth + Eyes & 0.71 & 0.83 & 0.96 \\
		2 & Face + Mouth + Eyes + Eyes/Mouth & 0.76 & 0.85 & 0.92 \\
		3 & Face + Mouth + Eyes + Features & 0.7 & 0.83 & 0.92 \\
		4 & Face + Mouth + Eyes + Eyes/Mouth + Features & 0.77 & 0.86 & 0.93 \\
		\bottomrule
	\end{tabular}
	\caption{Risultati dell'esperimento Multi Stacked Neural Network}
	\label{table:exp03_results}
\end{table}

\begin{figure}
	\centering
	\includegraphics[width=12cm]{images/03_results_accuracies.png}
	\caption{Confronto tra i valori di accuratezza ottenuti nell'esperimento 03}
	\label{figure:exp03_accuracies_all}
\end{figure}

Dopo aver visto i risultati globali, vediamo ora nel dettaglio una delle sessioni.
Prendiamo per esempio la sessione 04 che comprende tutte e cinque le reti nello step-1.
L'obiettivo in questo caso è comparare l'accuratezza ottenuta dalla rete Multi Stacked con quelle ottenute dalle singole reti che la compongono per poter meglio comprendere se c'è una correlazione con queste ultime.

Nella tabella \ref{table:exp03_results_05} vengono presentati i risultati, mentre in figura \ref{figure:exp03_accuracies} possiamo osservare meglio la distribuzione delle accuratezze.

\begin{table}[!htbp]
	\centering
	\begin{tabular}{c|ccc}
		\toprule
		Rete & \multicolumn{3}{c}{Accuratezza} \\
		{} & Minimo & Media & Massimo \\
		\midrule
		Face & 0.63 & 0.76 & 0.88 \\
		Eyes & 0.58 & 0.71 & 0.84 \\
		Mouth & 0.56 & 0.69 & 0.82 \\
		Eyes/Mouth & 0.67 & 0.77 & 0.86 \\
		Features & 0.5 & 0.57 & 0.67 \\
		Sessione 04 & 0.77 & 0.86 & 0.93 \\
		\bottomrule
	\end{tabular}
	\caption{Dettaglio dei risultati dell'esperimento Multi Stacked Neural Network per la sessione 4}
	\label{table:exp03_results_05}
\end{table}

\begin{figure}
	\centering
	\includegraphics[width=12cm]{images/03_results_accuracies_05.png}
	\caption{Confronto nel dettaglio tra i valori di accuratezza ottenuti dalla sessione 4}
	\label{figure:exp03_accuracies}
\end{figure}

Possiamo notare come l'accuratezza media della rete Multi Stacked sia decisamente migliore rispetto a qualsiasi sotto-rete presa singolarmente.
Questo ci dimostra che l'architettura ha superato positivamente la prova e riesce a unire i vantaggi delle singole reti anche in presenza di elementi che, con la propria accuratezza molto bassa, avrebbero potuto avere un effetto negativo sui risultati.
Un caso emblematico in questo senso è la classe Sad dove le feature, con la loro accuratezza al di sotto dello 0.2, non penalizzano la rete che, anzi, anche in presenza di tale rete, supera i valori ottenuti dalle singole reti.

Nel grafico in figura \ref{figure:exp03_accuracies_per_class} viene visualizzata l'accuratezza media per ogni classe.
Possiamo analizzare così se e quali classi sono favorite da questa tecnica.
Notiamo subito che quasi tutte le classi ne beneficiano in modo apprezzabile, a parte le due classi \emph{Happy} e \emph{Surprised}, le quali già ottengono valori medi piuttosto alti anche senza Multi Stacked.

Questi risultati ci confortano e provano la bontà della rete Multi Stacked che riesce a scoprire nuovi pattern utili allo scopo della classificazione.

\begin{figure}
	\centering
	\includegraphics[width=15cm]{images/03_05_per_class.png}
	\caption{Risultati medi della sessione 4 per classe}
	\label{figure:exp03_accuracies_per_class}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=8cm]{images/03_05_accuracy_overfitting.png}
	\caption{Andamento dell'accuratezza durante l'addestramento della Multi Stacked.}
	\label{figure:exp03_overfitting}
\end{figure}

Nonostante le buone prestazioni, è d'obbligo mettere in evidenza un generale problema di overfitting della rete dello step-2.

In figura \ref{figure:exp03_overfitting} viene messo in evidenza quanto rapidamente l'accuratezza sul training set va al suo valore massimo, mentre l'accuratezza sul validation set fluttua in modo regolare dieci punti percentuali al di sotto.

\section{Studio sulla resilienza della Multi Stacked NN}

Il precedente esperimento ha mostrato le potenzialità espresse da una configurazione elaborata come la Multi Stacked.

Lo studio non si vuole fermare alla sola analisi dei risultati in casi favorevoli, ma approfondire anche il suo comportamento sotto stress.
In particolare, un nuovo esperimento si propone di inserire un elemento di forte rumore con lo scopo di impedire, o per lo meno limitare, la correttezza della predizione finale.

Per simulare questa condizione sfavorevole è stata sostituita, all'atto della predizione, una delle reti dello step-1 con un generatore di numeri casuale.
La fase di training invece è stata mantenuta inalterata.
Sull'uscita della rete così alterata avremo sempre sette valori compresi nell'intervallo tra zero e uno, ma questa volta generati in modo casuale, con la caratteristica di avere sei dei sette valori vicino allo zero e il restante valore oltre la soglia dello $0.7$.
La classe rappresentata dal valore più alto sarà scelta in modo casuale tra le sette e rappresenterà la sua predizione.
Questo vuol dire che non ci concentreremo sulla simulazione di una situazione di incertezza, ma invece una forte certezza verso la classe sbagliata, amplificandone l'effetto negativo.

\'E stata scelta la rete della sessione 01 per l'esperimento, la quale contiene tre reti nello step-1: Face + Mouth + Eyes.

Sostituendo la rete Eyes con il generatore casuale possiamo comparare i risultati ottenuti sia con la sessione 01 che con la sessione 00 (Face + Mouth) dove sono presenti solo Face e Mouth.

In tabella \ref{table:exp03_results_07} vediamo i risultati ottenuti.
Come ci potevamo immaginare, il generatore casuale fa perdere il 7\% delle prestazioni.
La media scende così poco sotto a quella ottenuta nella sessione 00.
Questo significa che, nel caso peggiore, la rete riesce comunque a isolare ed escludere piuttosto efficientemente la fonte dell'errore.

\begin{table}[!htbp]
	\centering
	\begin{tabular}{c|ccc}
		\toprule
		Rete & \multicolumn{3}{c}{Accuratezza} \\
		{} & Minimo & Media & Massimo \\
		\midrule
		Face + Mouth & 0.66 & 0.78 & 0.89 \\
		Face + Mouth + Eyes & 0.71 & 0.83 & 0.96 \\
		Face + Mouth + Eyes (fonte di errore) & 0.55 & 0.76 & 0.87 \\
		\bottomrule
	\end{tabular}
	\caption{Analisi della Multi Stacked con Error Injection sulla rete che elabora gli occhi.}
	\label{table:exp03_results_07}
\end{table}

Abbiamo così capito come la rete reagisce a situazioni inverse.
Nella tabella \ref{table:exp03_results_07_2} viene invece evidenziato quale sotto-rete influisce in modo più negativo.
Come ci si poteva aspettare, Face ha una influenza maggiore rispetto alle altre sotto-reti, che provocano variazioni simili.

\begin{table}[!htbp]
	\centering
	\begin{tabular}{c|ccc}
		\toprule
		Rete & \multicolumn{3}{c}{Accuratezza} \\
		{} & Minimo & Media & Massimo \\
		\midrule
		Face + Mouth + Eyes & 0.71 & 0.83 & 0.96 \\
		Face (fonte di errore) + Mouth + Eyes & 0.56 & 0.72 & 0.85 \\
		Face + Mouth (fonte di errore) + Eyes & 0.55 & 0.76 & 0.87 \\
		Face + Mouth + Eyes (fonte di errore) & 0.64 & 0.76 & 0.87 \\
		\bottomrule
	\end{tabular}
	\caption{Comparazione dell'accuratezza della Multi Stacked con differenti Error Injection.}
	\label{table:exp03_results_07_2}
\end{table}