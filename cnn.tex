\section{Reti neurali}

Le reti neurali artificiali (ANN) sono sistemi computazionali che si ispirano al modello biologico dei neuroni e delle sinapsi del cervello.

Questo tipo di sistemi ``impara'' ad eseguire operazioni attraverso una fase di addestramento dove vengono presentati esempi di input associati agli output che ci si aspetta di ottenere in loro corrispondenza.

\begin{figure}
	\centering
	\includegraphics[width=4cm]{images/ann.png}
	\caption{Esempio di rete neurale}
	\label{figure:ann}
\end{figure}

Una rete neurale è solitamente composta da una collezione di unità, chiamati neuroni artificiali, che modellano i neuroni in un cervello biologico.
I neuroni sono connessi fra loro da ``sinapsi'' che trasmettono un segnale da un neurone all'altro.
Il neurone che riceve segnali da una serie di sinapsi potrà così elaborare queste informazioni e inviare il risultato ai neuroni cui è connesso a sua volta.

In figura \ref{figure:ann} viene esposto un esempio di rete neurale.

Ad ogni segnale in ingresso viene associato un peso che viene modificato attraverso il processo di apprendimento.
Solitamente in uscita viene posta una funzione non lineare chiamata funzione di attivazione.

In linguaggio matematico, un neurone può essere descritto come una funzione $f:X \rightarrow Y$, dove ad una funzione di attivazione ``A'' viene passata la somma pesata di tutti gli input $g_{i}(x)$.

Normalmente, viene inserito anche un valore costante $b \in R$, detto ``Bias'', utile per spostare verso destra o verso sinistra la funzione di attivazione.

\begin{equation}\label{ann_output}
f(x) = A (\sum_i{w_i g_{i}(x)} + b)
\end{equation}

Il processo di apprendimento della rete consiste in un algoritmo che aggiusta i pesi $w_{i}$ in modo da produrre un output coerente con gli esempi forniti.

\subsection{Backpropagation}

L'algoritmo di backpropagation è una tecnica per l'addestramento di una rete neurale.

Essa prevede due fasi: una in avanti (forward) in cui viene presentato alla rete un esempio, determinata l'uscita e calcolato l'errore rispetto all'uscita attesa; una fase indietro (backward) in cui l'errore viene propagato nella rete e i pesi vengono progressivamente aggiustati.

\subsection{Funzioni di attivazione}

La funzione di attivazione di un nodo definisce l'output di quest'ultimo.

La somma pesata degli ingressi può essere un valore che varia da $-\inf$ a $+\inf$ e la funzione di attivazione ci permette di definire quando attivare l'uscita o meno.

Normalmente viene utilizzata una funzione non lineare che rende la rete capace di eseguire compiti più complessi rispetto a semplici modelli di regressione lineare.

\begin{equation}\label{fun_act_output}
y = \sum_i{w_i g_{i}(x)} + b
\end{equation}

In seguito sono descritte le funzioni più popolari:

\begin{description}
	\item[Binary Step Function] Definita come 
	\begin{equation}
	f(x) = 1, x \geq 0
	\end{equation}
	\'E la funzione più semplice di threshold. Se il valore di Y è sopra una data soglia, allora attiva il neurone, altrimenti lo lascia inattivo.
	La funzione è estremamente semplice e può essere usata per creare un classificatore binario, anche se la funzione è più teorica che pratica, in quanto il gradiente della funzione gradino è zero e quindi difficile da usare siccome algoritmi di back-propagation utilizzano il gradiente per ottimizzare l'uscita.
	\item[Sigmoid] Simile alla funzione a gradino, ma con ha un andamento più morbido. Descritta dalla funzione:
	\begin{equation}
	f(x) = \frac{1} {1 + e^{-x}}
	\end{equation}
	L'output sarà un valore compreso tra zero e uno. Nelle due estremità della sigmoide, il valore di Y tende a variare sempre meno rispetto a X, generando il problema del ``vanishing gradient'': la rete si rifiuta di imparare, cioè modifica i pesi in modo irrilevante.
	
	Nonostante questo, ci sono dei modi per aggirare il problema e continua ad essere molto popolare in problemi di classificazione.
	\item[Tangente iperbolica] Definita come:
	\begin{equation}
	tanh(x) = \frac{1 - e^{-2x}} {1 + e^{-2x}}
	\end{equation}
	Viene utilizzata quando è necessaria una funzione di attivazione nell'intervallo $[-1, 1]$.
	\item[ReLu] La funzione Rectified Linear Unit è definita come
	\begin{equation}
	f(x) = max(0, x)
	\end{equation}
	La caratteristica principale di questa funzione è che risolve il problema del ``vanishing gradient''. Inoltre, velocizza il processo di convergenza.
	
	Il problema maggiore si presenta quando il gradiente accumulato ha segno negativo, con la conseguenza che i pesi non vengono aggiornati.
	\item[Leaky ReLu] Questa funzione è un tentativo di risolvere il problema della disattivazione dei neuroni della ReLu e consiste nell'introduzione di una piccola pendenza negativa nella regione dove la ReLu è nulla.
	\item[Softmax] Generalizzazione della funzione logistica, ha la caratteristica di schiacciare gli output di ciascuna unità tra 0 e 1, proprio come una sigmoide. Inoltre, divide anche ciascuna uscita in modo tale che la somma totale delle uscite sia uguale a 1, proprio come una probabilità.
\end{description}

\subsection{Algoritmi di ottimizzazione}

L'algoritmo di ottimizzazione viene utilizzato insieme al metodo di retropropagazione dell'errore per eseguire l'addestramento delle reti.
Vediamo di seguito quali sono i più utilizzati:

\begin{description}
	\item[SDG] La discesa stocastica del gradiente (SDG) \cite{stochastic_estimation} è un metodo iterativo per l'ottimizzazione di funzioni differenziali, è una approssimazione stocastica del metodo di discesa del gradiente quando la funzione costo ha la forma di una somma.
	L'algoritmo sostituisce l'uso del valore esatto del gradiente della funzione costo con una stima ottenuta valutando il gradiente solo su un sottoinsieme degli addendi.
	
	Ad oggi è l'algoritmo considerato lo standard de facto per l'addestramento.
	\item[AdaGrad] (adaptive gradient) usa un tasso di apprendimento indipendente per ogni parametro. Intuitivamente, questo metodo usa tassi di apprendimento più elevati per parametri con una distribuzione più sparsa, e più piccoli per i parametri con distribuzione più densa. 
	
	Il principale svantaggio è la tendenza del metodo a cancellare il learning rate nel corso delle iterazioni, portando ad un arresto dell'apprendimento.
	\item[RMSProp] (Root Mean Square Propagation) è un metodo con tasso di apprendimento adattivo che affronta il problema della cancellazione del tasso di apprendimento in AdaGrad.
	\item[AdaDelta] Come RMSProp, è una variante di AdaGrad che contrasta la cancellazione del tasso di apprendimento. È stato sviluppato indipendentemente da RMSProp, di cui può essere considerato una estensione.
	\item[Adam] (Adaptive moment estimation) \cite{2014arXiv1412.6980K} è un'estensione di RMSProp che tiene conto della media mobile dei momenti di primo e secondo ordine del gradiente. Viene descritto dal suo autore come combinazione dei vantaggi di AdaGrad e RMSProp.
	
	Risultati empirici dimostrano che Adam converge prima rispetto agli algoritmi precedentemente descritti. 
	Nell'immagine \ref{figure:adam} viene visualizzato il costo dell'addestramento sul dataset MNIST, comparando così gli altri algoritmi e mettendo in evidenza le potenzialità di Adam.
\end{description}

\begin{figure}
	\centering
	\includegraphics[width=7cm]{images/adam.png}
	\caption{Comparazione tra vari algoritmi di ottimizzazione.}
	\label{figure:adam}
\end{figure}

\subsection{Funzione di loss}

La funzione di loss in Machine Learning rappresenta la misura dell'inesattezza della predizione in problemi di classificazione o regressione.
Questo metodo viene usato per valutare quanto bene un algoritmo modelli il dataset.

Se la predizione è completamente sbagliata, il suo valore sarà molto alto.
Al contrario, più la predizione è buona e più il suo valore convergerà verso zero.

Di seguito sono presentate le funzioni di loss classiche:

\begin{description}
	\item[MSE] La funzione più comunemente usata è la Mean Squared Error (MSE) in problemi di regressione perché facile da implementare e da capire. Generalmente funziona bene.
	\begin{equation}
	MSE = \frac{1}{n} \sum_{i=1}^{n}(\hat{X}_i - X_i)^2
	\end{equation}
	
	Dove $\hat{X_i}$ è l'uscita ottenuta e $X_i$ è l'uscita che si desidera ottenere.
	\item[Cross entropy loss] (Log Loss) è comunemente usata per problemi di classificazione.
	
	La funziona è strettamente correlata alla \emph{Kullback–Leibler divergence} che è una misura di quanto una distribuzione di probabilità sia differente rispetto a una seconda.
	
	\begin{equation}
		L = -y \cdot log(\hat{y})
	\end{equation}
	
	Dove Y è l'uscita attesa e $\hat{Y}$ è l'uscita ricostruita dalla rete.
\end{description}

Ovviamente, esistono anche altre funzioni più complesse che sono state descritte in letteratura solitamente per problemi più specifici.

Tra queste citiamo la \emph{triplet-loss} \cite{journals/corr/SchroffKP15}, utilizzata per discriminare i volti delle persone.

\begin{equation}\label{Triplet-Loss}
\sum_{i}^{N} [\left \| f(x_i^a) - f(x_i^p)  \right \|_2^2 - \left \| f(x_i^a) - f(x_i^n)  \right \|_2^2 + \alpha]
\end{equation}

Prese tre immagini, due delle quali della stessa persona e una di un soggetto diverso, la funzione ha la caratteristica di minimizzare la distanza tra le prime due, massimizzando invece quella tra la prima e l'ultima.

\begin{figure}
	\centering
	\includegraphics[width=10cm]{images/cnn.png}
	\caption{La Triplet-Loss minimizza la distanza tra l'\emph{ancora} e una \emph{positiva}, entrambe della stessa persona, a massimizza la distanza tra l'\emph{ancora} e una \emph{negativa} di una persona diversa.}
	\label{figure:cnn}
\end{figure}

\subsection{Forme di regolarizzazione}

Il problema principale di un algoritmo di Machine Learning è la generalizzazione, ossia che si comporti bene non solo sul training set ma anche su nuovi input non utilizzati in fase di training.

Vengono chiamate tecniche di regolarizzazione tutte quelle tecniche che permettono all'algoritmo di apprendimento di ridurre l'errore di generalizzazione ma non l'errore sull'addestramento.
Quindi, ridurre l'errore sul set di test anche a costo di aumentare l'errore sul training.

Quello che si vuole evitare è che la rete vada in \emph{overfitting}, ossia la rete si adatta fin troppo bene ai dati di addestramento, perdendo capacità di generalizzare.

L'overfitting può essere riscontrato in situazioni in cui il valore della funzione di loss diminuisce ad ogni epoca di addestramento per i dati di training, mentre aumenta per i dati di validazione.

Le strategie più utilizzate sono: 

\begin{description}
	\item[L1 regularization] Aggiunge una penalità L1 uguale al valore assoluto della magnitudo dei coefficienti.
	\item[L2 regularization] Aggiunge una penalità uguale al quadrato della grandezza dei coefficienti.
	\item[L1+L2 regularization] Combina i metodi L1 e L2.
	\item[Early Stopping] Ogni ciclo della fase di training deve essere accompagnata da una fase di valutazione su un dataset terzo, detto validation set. Se l'errore sul validation set non diminuisce o peggiora, il training viene bloccato.
	\item[Dropout] Ad ogni epoca nella fase di addestramento, i singoli nodi vengono ''eliminati'' dalla rete con probabilità $1-p$ o mantenuti con probabilità $p$, in modo che venga utilizzata una rete di dimensioni ridotte.
	\item[Data Augmentation] Questa tecnica prevede di creare nuovi esempi da quelli in possesso in modo da aumentare la dimensione del training set.
	\item[Fine-tuning] Per applicazioni in cui la quantità di dati per l'addestramento è poca, una tecnica comune è di addestrare la rete su un dataset molto grande in un dominio correlato. Una volta che i parametri convergono, un ulteriore passo di addestramento viene fatto utilizzando i dati all'interno del dominio per mettere a punto i pesi della rete.
\end{description}


\section{Convolution layers}

Hubel e Wiesel (1962) scoprono la presenza, nella corteccia visiva del gatto, di due tipologie di neuroni:

\begin{enumerate}
	\item Simple cells: agiscono come feature detector locali fornendo selettività
	\item Complex cells: fondono (pooling) gli output di ``simple cell'' in un intorno, garantendo invarianza
\end{enumerate}

Nel 1980, Fukushima modella il Neocognitron, una delle prime reti neurali che cerca di modellare questo comportamento.

Solo nel 1998, Lecun \cite{cnn} introduce le prime Convolutional Neural Networks, descrivendo l'architettura di LeNet.
Le principali differenze rispetto alle Multi Layer Networks sono:

\begin{description}
	\item[processing locale] I neuroni sono connessi solo localmente ai neuroni del livello precedente. Ogni neurone esegue quindi una elaborazione locale con una forte riduzione del numero di connessioni.
	\item[pesi condivisi] I pesi sono condivisi a gruppi. Neuroni diversi dello stesso livello eseguono lo stesso tipo di elaborazione su porzioni di input diverso portando a una forte riduzione del numero di pesi.
	\item[alternanza] Alternanza di livelli di convoluzione e pooling.
\end{description}

\begin{figure}
	\centering
	\includegraphics[width=10cm]{images/cnn_kernel.png}
	\caption{Operazione di convoluzione su una immagine}
	\label{figure:cnn_kernel}
\end{figure}

Le reti convolutive sono state progettate con il preciso compito di processare immagini.
Per questo motivo, il livello di input, solitamente, viene direttamente connesso ai pixel dell'immagine.

Come si vede in figura \ref{figure:cnn_kernel}, i blocchi di neuroni che condividono una connessione possono essere visti come applicazione di un kernel alla rappresentazione dell'immagine nello strato precedente.

Il kernel si può vedere come matrice di pesi $w_i$ che viene applicata a una porzione di immagine $x_i$.
La sua applicazione viene descritta dalla seguente equazione:

\begin{equation}
	y = \sum_{i} w_i \cdot x_i
\end{equation}

\begin{figure}
	\centering
	\includegraphics[width=10cm]{images/cnn.png}
	\caption{Esempio di rete convolutiva}
	\label{figure:cnn}
\end{figure}

In figura \ref{figure:cnn} si intuisce come il campo visivo dei neuroni aumenta muovendosi verso l'alto della gerarchia.
Più si va verso la fine e più la rete estrae feature astratte.

I neuroni sono organizzati in griglie 3D, dove su ogni piano di grandezza \emph{width x height} si conserva l'organizzazione spaziale dell'immagine di input.
La terza dimensione denominata \emph{depth} individua le diverse \emph{feature map}.

\begin{figure}
	\centering
	\includegraphics[width=14cm]{images/feature_map.png}
	\caption{Rete LeNet}
	\label{figure:cnn_feature_map}
\end{figure}

Prendiamo come esempio il primo strato di convoluzione in figura \ref{figure:cnn_feature_map}: lo strato si compone di 6 features map.
Ogni unità in ogni feature map è connessa a 5x5 pixel vicini nell'immagine di input, con uno ``stride'' 1x1.
Le feature in uscita saranno 6, composte ciascuna da una griglia di 28x28 neuroni.

In totale, questo strato si compone di $156 = 6 x (5x5x1 + 1)$ neuroni, dove l'1 rappresenta il bias.
Il numero totale di connessioni è dato invece da $ 117600 = (28 x 28 x 6) x (5x5x1) $.
Per una rete ``fully connected'' avremmo invece $ = (28 x 28 x 6) x (32x32) $, decisamente minore se la  dove ogni neurone è connesso con tutti i neuroni dello strato precedente.

Definiamo di seguito i parametri comuni a tutti i convolutional layer:

\begin{description}
	\item[kernel size (width x height)] I kernel più comunenemente usati sono di grandezza 1x1, 3x3, 5x5. Rappresentano il numero di connessioni in ingresso di ogni neurone, ossia, a quanti pixel dell'immagine, o neuroni se si considera uno strato intermedio, è connesso. Ad ogni connessione è associato un peso che nella fase di addestramento verrà calibrato e che, in questo modo, cambierà la forma della convoluzione finale.
	\item[depth] Identifica quante feature map vengono estratte dal layer. Ogni feature map può essere vista come il risultato di uno specifico filtraggio dell'input.
	\item[stride] Questo parametro controlla come il filtro convolve attorno all'input. Nel caso di stride 1 come in figura \ref{figure:stride1}, il filtro si sposta di una unità alla volta, avendo come risultato in output una feature delle stesse dimensioni, o quasi.
	Se invece si usasse uno stride di 2 come in figura \ref{figure:stride2}, la dimensione della feature in output si dimezza rispetto a quella di input.
	\item[padding] Per regolare la dimensione delle feature map è anche possibile aggiungere un bordo con valori nulli al volume di input.
	
	Per esempio, nella rete LeNet, applicare un filtro 5x5 su una immagine originale 32x32 ha fatto sì che in output la feature map si riducesse a una matrice 28x28. 
	Se volessimo mantenere la stessa dimensione dell'input, possiamo aggiungere un padding di 2 come in figura \ref{figure:padding}.
\end{description}

Possiamo ricavare la dimensione $d_{out}$ (width o heigth) della feature map applicando la seguente formula:

\begin{equation}
	d_{out} = \frac{d_{in} - d_{kernel} + 2 \cdot Padding}{stride} + 1
\end{equation}

Dove $d_{in}$ rappresenta la dimensione di input, $d_{kernel}$ la dimensione del kernel.
Nell'esempio preso in esame precedentemente che riguarda il primo strato della \emph{LeNet}, abbiamo:

\begin{itemize}
	\item stride = 1
	\item padding = 0
	\item $d_{in}$ = 32
	\item $d_{kernel}$ = 5
	\item $d_{out}$ = 28
\end{itemize}

\begin{figure}
	\centering
	\includegraphics[width=9cm]{images/stride1.png}
	\caption{Esempio di filtro con stride 1}
	\label{figure:stride1}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=9cm]{images/stride2.png}
	\caption{Esempio di filtro con stride 2}
	\label{figure:stride2}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=4cm]{images/pad.png}
	\caption{Esempio di filtro con padding 2}
	\label{figure:padding}
\end{figure}

\section{Pooling Layers}

Lo scopo di questo strato è quello di diminuire la dimensione delle feature map mantenendo le informazioni più importanti.

Come nel caso di una convoluzione viene applicato un kernel, il cui stride solitamente è grande quanto il kernel stesso. 
Questo ha l'effetto di diminuire la dimensione della feature originale, rendendola più piccola, riducendo così il numero totale di parametri.

Questa aggregazione delle informazioni ha il compito di aiutare a generalizzare, riducendo la probabilità di overfit perché i dettagli vengono persi durante la fase di pooling.

A differenza di uno strato convolutivo, qui la funzione del kernel è diversa.
Vediamo di seguito i tipi più comunemente usati:

\begin{description}
	\item[Max Pooling] La funzione del kernel esegue una operazione di max.
	Ossia, come si vede in figura \ref{figure:maxpooling} avrà come output il valore massimo tra quelli della matrice in input.
	\item[Average Pooling] Il valore in uscita sarà il valore medio tra i valori della matrice in ingresso.
	\item[Sum Pooling] Il valore di uscita sarà la somma dei valori della matrice in ingresso.
\end{description}

\begin{figure}
	\centering
	\includegraphics[width=8cm]{images/MaxPool.png}
	\caption{Esempio di Max Pooling 2x2 con stride 2.}
	\label{figure:maxpooling}
\end{figure}

\section{Fully Connected Layer}

Questo tipo di livello si comporta come una classica rete neurale dove ogni neurone di un livello è connesso a tutti i neuroni del livello precedente.

Lo scopo principale è la classificazione e viene di solito usato come ultimo tassello di una Convolutional Neural Network.

\section{Architettura Inception}

La scelta della tipologia di filtri da usare nei vari strati è una operazione cruciale e non è una decisione facile da prendere.

Intuitivamente, utilizzare filtri con un kernel più grande o più piccolo aiutano a estrarre feature di scale diverse.
Ma non è detto che nelle immagini la scala sia sempre rispettata, rendendo la rete sensibile a come gli oggetti si presentano nell'immagine.

Szegedy et all \cite{2014arXiv1409.4842S} tentano di risolvere questo problema applicando in modo originale il concetto di Network in a Network proposto da Lin at all \cite{2013arXiv1312.4400L}.
Cioè, utilizzare in parallelo filtri di diverse dimensioni e lasciare alla rete il compito di decidere quale usare.

Come mostrato in figura \ref{figure:inception}, viene così definito il modulo base \emph{Inception} come composizione di filtri 1x1, 3x3, 5x5 e un 3x3 Max Pooling.

\begin{figure}
	\centering
	\includegraphics[width=10cm]{images/inception.png}
	\caption{Architettura del modulo Inception proposto nel paper \cite{2014arXiv1409.4842S}}
	\label{figure:inception}
\end{figure}

La concatenazione dei filtri convolutivi porta ad una inevitabile esplosione del numero degli output da ogni stage.
Rendendo il costo computazionale sempre più alto ed esplodendo dopo pochi stage.

Per mantenere sotto controllo questo problema, filtri convoluzionali 1x1 sono stati usati per ridurre la dimensione degli output.
In pratica, il filtro lascia inalterato il contenuto e le dimensioni \emph{width x height}, agendo sulla dimensione \emph{depth}, modificandola a piacere.
Il suo scopo è quindi la compressione del volume di input.

\section{Stacked Neural Networks}

È dimostrato in letteratura \cite{chen2014learning} che un insieme di più reti indipendenti può migliorare l'accuratezza della previsione riducendo il tasso di errore di classificazione.

La tecnica di ``transfer learning'' è un metodo di apprendimento automatico in cui un modello sviluppato per un'attività viene riutilizzato come punto di partenza per un modello specializzato in una seconda attività.

Razavian et al. \cite{sharif2014cnn} hanno dimostrato che l'apprendimento eseguito a partire dai risultati prodotti da più reti pre-addestrate sullo stesso problema può fornire risultati migliori rispetto alle singole reti addestrate.

In \cite{mohammadi2016snn} viene suggerito così di combinare diverse reti per migliorare la classificazione compensando le mancanze delle singole reti.

\begin{figure}
	\centering
	\includegraphics[width=11cm]{images/workflow_multistacked.png}
	\caption{Architettura di una Stacked Neural Network.}
	\label{figure:multi_stacked_schema}
\end{figure}

Vediamo le caratteristiche dell'architettura visualizzata in figura \ref{figure:multi_stacked_schema}:

\begin{itemize}
	\item Ci sono M modelli addestrati ognuno in modo differente sul training set $X$.
	\item Ogni modello fornisce una predizione $\hat{Y}$.
	\item Le predizioni vengono così accorpate per formare i dati di training $X^{(l2)}$ per una rete di secondo livello.
	\item La rete di secondo livello può essere addestrata su questo nuovo training set, producendo il risultato finale usato come predizione.
\end{itemize}

\section{Hyperparameters}

Un modello di \emph{Machine Learning} può essere visto come la definizione di una formula matematica con un numero di parametri che devono essere appresi dai dati. Il punto cruciale è quindi l'adattamento di un modello ai dati. Questo viene fatto attraverso un processo noto come addestramento del modello. In altre parole, a partire dai dati esistenti, siamo in grado di adattare i parametri del modello per ottenere il comportamento desiderato.

Tuttavia, esistono altri parametri che non possono essere appresi direttamente dal normale processo di addestramento. Questi parametri esprimono proprietà ``di livello superiore'' del modello, come la sua complessità o parametri da cui dipende la velocità con cui dovrebbe apprendere. Sono chiamati \emph{hyperparameters}. 
Gli iperparametri vengono solitamente impostati prima che inizi il vero processo di addestramento.

Vediamo alcuni esempi di iperparametri:

\begin{itemize}
	\item Tasso di apprendimento
	\item Numero di strati della rete
	\item Numero di epoche di addestramento
	\item Batch size
	\item Parametri dell'algoritmo di ottimizzazione 
	\item Learning Rate e Momentum
	\item Inizializzazione dei pesi della rete
	\item Funzione di attivazione dei neuroni
	\item Configurazione delle regolarizzazioni
\end{itemize}

Le tecniche classiche utilizzate per trovare la configurazione migliore sono:

\begin{description}
	\item[Ricerca manuale] Sta a chi investiga impostare ipotesi che lo guideranno nella ricerca di tipo ``trial and error'' dei iperparametri ottimali.
	\item[Grid Search] Una ricerca esauriente attraverso la specifica manuale di un sottoinsieme dello spazio iperparametrico. L'algoritmo di ricerca in griglia è guidato da alcuni parametri di prestazione.
	\item[Random Search] \cite{bergstra2012random} Metodo di ricerca casuale che empiricamente può essere più efficiente di una ricerca su griglia.
	\item[Bayesian optimization] Modellazione \cite{NIPS2012_4522} delle prestazioni di un algoritmo di apprendimento come un processo gaussiano.
\end{description}