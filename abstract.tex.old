\chapter*{Abstract}
\markboth{Abstract}{Abstract}
\addcontentsline{toc}{chapter}{Abstract}

Il riconoscimento di emozioni è un tema di ricerca molto attuale e ancora in piena evoluzione.

Identificare un  numero di emozioni universali che possano costituire una base da cui derivare tutte le altre come loro "specializzazioni" è un  tema ancora  controverso.
Uno  degli studi maggiormente accettato nella comunità scientifica \`e il lavoro degli psicologi Ekman e Friesen che identificano sette emozioni universali: rabbia, disgusto, paura, felicità, tristezza, sorpresa, neutrale.

La tesi si propone come obiettivo l'elaborazione di immagini contenenti facce di persone e l'identificazione dell'emozione espressa in esse.
L'attenzione principale è rivolta all'individuazione dell'architettura di deep learning migliore.

Per l'occasione è stato creato  il progetto RemoKit (Recognize Emotion Toolkit), ossia un insieme di strumenti atti a facilitare la creazione di esperimenti e la successiva analisi dei risultati prodotti.  

Sono stati eseguiti diversi esperimenti per valutare come procedere nella progettazione della rete.

La validazione del modello avviene sempre attraverso l'uso di tecniche statistiche che minimizzano la dipendenza dei risultati dalle caratteristiche del campione preso in esame, quali la  K-Fold Cross Validation e ripetizione multipla di esperimenti indipendenti, ciascuno dei quali viene considerato come istanza di una variabile casuale, eseguiti cambiando ogni volta il seme del generatore pseudo-casuale.

Per ovviare al problema della grandezza del dataset, sono stati implementati metodi di pre-processing che permettono di omogeneizzare le immagini provenienti da dataset diversi.
Sono stati selezionati i dataset più comunemente utilizzati: KDEF, CK+, FED, JAFFE e PICS.

Le foto sono state scattate in un ambiente controllato e in ogni immagine è presente una unica persona che esprime una sola emozione.

I risultati, in presenza di dataset di dimensione limitata, suggeriscono l'uso di una rete composta da tre livelli convolutivi seguita da una rete di classificazione.
Inoltre, è stata evidenziata la crescente capacità di generalizzazione in caso il dataset aumenti.

Successivamente è stata osservata la quantità di informazione disponibile nelle singole parti il viso: gli occhi, la bocca e l'insieme dei landmarks che descrivono il volto. Confrontandoli con i risultati dell'elaborazione del viso intero.
La domanda a cui si voleva rispondere è: quale parte del viso è più espressiva?

Le metriche ricavate ci suggeriscono l'alto potenziale espressivo degli occhi. 
Non sorprende invece che i risultati ottenuti dalla sola bocca siano di qualità molto inferiore. 
Ciononostante, si evidenzia come le emozioni percepibili non siano del tutto le stesse.

I migliori risultati sono stati ottenuti elaborando l'intera faccia.
Questo mette in evidenza la capacità della rete di imparare a ignorare le parti dell'immagine che non contengono
informazioni utili. Sono state usate tecniche di Saliency Maps e grad-CAM per visualizzare dove tende a focalizzarsi l'attenzione della rete nelle immagine di input.

L'esperimento ha suggerito l'uso delle "multi-stacked neural networks".
Per ogni parte del viso e per il viso stesso è stata addestrata una rete.
Le uscite delle diverse reti sono state concatenate e utilizzate come input di una ulteriore rete "fully connected", il cui compito è fare una sintesi ed esprimere a sua volta un giudizio finale. 
Le metriche ottenute confermano un andamento crescente dell'accuratezza all'aumentare del numero delle sotto-reti, dimostrando che la cooperazione ha effetti positivi. Notiamo inoltre che l'accuratezza è sempre migliore rispetto alle singoli sotto-reti.

L'ultimo esperimento riguarda l'uso delle sequenze di immagini di ogni soggetti. L'espressione varia da neutra fino a una determinata emozione. La rete è stata in grado di percepire la graduali variazione dell'espressione, ricostruendo la sequenza nella maggior parti dei casi.